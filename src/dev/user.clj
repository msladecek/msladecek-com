(ns user
  (:require
   [optimus.optimizations :as optimizations]
   [optimus.prime :as optimus]
   [optimus.strategies :refer [serve-live-assets]]

   [clojure.edn :as edn]
   [org.httpkit.server :refer [run-server]]
   [stasis.core :as stasis]
   [msladecek-com.build :refer [all-pages all-assets config]]))


(def app
  (optimus/wrap (stasis/serve-pages
                 #(all-pages config)
                 {:config (edn/read-string (slurp "content/structure.edn"))})
                all-assets
                optimizations/none
                serve-live-assets))


(defonce server (atom nil))


(defn start-server []
  (reset! server (run-server app {:port 8000})))


(defn stop-server []
  (when-not (nil? @server)
    (@server :timeout 100)
    (reset! server nil)))


(defn restart-server []
  (stop-server)
  (start-server))


(comment
  (restart-server)


  (stop-server)

  )
