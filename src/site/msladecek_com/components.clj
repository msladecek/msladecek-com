(ns msladecek-com.components
  (:require
   [java-time :as jt]
   [clojure.string :as str]))


(defn navigation-menu [context]
  (let [active-path-segment (-> (:uri context) (str/split #"/") second)]
    [:div.ui.inverted.large.attached.menu
     [:div.ui.container
      [:a.item
       {:href "/"}
       [:h2 (get-in context [:config :meta :website-title] "Home")]]
      [:div.right.menu
       (for [{:keys [path label]} (get-in context [:config :navigation-menu])]
         (let [cls (if (= path active-path-segment)
                     :a.item.active
                     :a.item)]
           [cls {:href (format "/%s/" path)} [:h3 label]]))]]]))


(defn profiles-menu
  ([context]
   (profiles-menu {:icon-size :medium} context))
  ([{:keys [icon-size]} context]
   [:div.ui
    (for [{:keys [icon link]} (get-in context [:config :profiles])]
      [:a.ui.tertiary.black.icon.button
       {:href link}
       [:i.icon {:class (if-not (= "medium" (name icon-size))
                          (str (name icon-size) " " icon)
                          icon)}]])]))


(defn footer [context]
  [:div.ui.footer.container
   [:div {:style "display: flex; flex-flow: row wrap-reverse; justify-content: space-around;"}
    [:div {:style "flex: 0 1 200px; text-align: center; display: flex; flex-direction: column; justify-content: center;"}
     [:p (get-in context [:config :meta :copyright-notice])]]
    [:div {:style "flex: 0 1 300px; text-align: center;"}
     (profiles-menu {:icon-size :large} context)]]])


(defn post-meta
  ([post-data]
   (post-meta post-data {:tag-fn identity}))
  ([{:keys [date tags reading-time-minutes]} {:keys [tag-fn]}]
   [:div.post-meta
    [:div {:style "display: flex; justify-content: space-between;"}
     [:span {:style "flex: 1;"}
      [:time {:datetime date}
       (jt/format "LLLL d, yyyy" (jt/local-date "yyyy-MM-dd" date))]]
     [:span.post-tag-list {:style "flex: 1; text-align: right;"}
      [:ul
       (for [tag tags] [:li (tag-fn tag)])]]]
    (when reading-time-minutes
      [:em (str reading-time-minutes " minutes")])]))
