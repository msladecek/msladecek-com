(ns msladecek-com.page-transformers
  (:require
   [net.cgrand.enlive-html :as enlive]
   [markdown.core :as markdown]
   [clygments.core :as pygments]
   [hiccup.page :refer [html5]]
   [msladecek-com.components :as components]
   [clojure.string :as str])
  (:import (org.jsoup Jsoup)))

(defn -strip-html-tags [string]
  (.text (Jsoup/parse string)))

(defn render-page [page-fn {:keys [menu-fn footer-fn title description]}]
  (fn [context]
    (html5
     [:head
      [:meta {:charset "utf-8"}]
      [:meta {:name "viewport"
              :content "width=device-width, initial-scale=1.0"}]
      (let [global-title (get-in context [:config :meta :website-title])
            page-title (if title
                         (str title " | " global-title)
                         global-title)]
        (list
         [:title page-title]
         [:meta {:property "og:title", :content page-title}]))
      (when description
        (list
         [:meta {:name "description"
                 :content description}]
         [:meta {:property "og:description"
                 :content description}]))
      [:link {:rel "preconnect" :href "https://fonts.googleapis.com"}]
      [:link {:rel "preconnect" :href "https://fonts.gstatic.com" :crossorigin true}]
      [:link {:href "https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" :rel "stylesheet"}]
      [:link {:rel "stylesheet" :href "/Fomantic-UI-CSS/semantic.min.css"}]
      [:link {:rel "stylesheet" :href "/pygments/bw.css"}]
      [:link {:rel "stylesheet" :href "/custom.css"}]
      [:script {:src "https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"}]
      [:script {:src "https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js" :defer true}]]
     [:body
      [:div.site-wrap
       (when menu-fn
         [:header
          (menu-fn context)])
       [:div.ui.main.container.site-content
        (page-fn context)]
       (when footer-fn
         [:footer
          (footer-fn context)])]])))

(defn- highlight-code [node]
  (let [code (apply str (:content node))
        lang (keyword (get-in node [:attrs :class]))
        highlighted-code (if-let [highlighted (pygments/highlight code lang :html)]
                           (-> highlighted
                               java.io.StringReader.
                               enlive/html-resource
                               (enlive/select [:pre])
                               first
                               :content)
                           code)]
    (assoc node :content highlighted-code)))

(defn highlight-code-blocks [page]
  (enlive/sniptest page
                   [:pre :code] highlight-code
                   [:pre :code] #(assoc-in % [:attrs :class] "highlight")))

(defn md-to-html-string [content]
  (markdown/md-to-html-string
   content
   :footnotes? true
   :reference-links? true
   :heading-anchors true))

(defn render-markdown [page]
  (fn [_]
    [:div.ui.text.container
     (-> page
         md-to-html-string
         highlight-code-blocks)]))

(defn render-markdown-post [page params]
  (fn [_]
    (let [page-content (md-to-html-string page)
          average-words-per-minute 220
          word-count (-> (-strip-html-tags page-content)
                         (str/split #"\w+")
                         count)
          reading-time-minutes (-> (/ word-count average-words-per-minute)
                                   Math/ceil
                                   int)]
      [:div.ui.text.container.markdown-post-content
       [:h1 (:title params)]
       (components/post-meta (assoc params :reading-time-minutes reading-time-minutes))
       (highlight-code-blocks page-content)])))
