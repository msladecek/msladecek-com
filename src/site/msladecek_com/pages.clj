(ns msladecek-com.pages
  (:require
   [clojure.data.json :as json]
   [msladecek-com.components :as components]))


(defn index [context]
  [:div.index-content
   [:div.ui.vertical.segment
    [:h1.ui.massive.center.aligned.header
     "Martin Sladecek"
     [:div.sub.header "Software Engineer"]]
    (components/profiles-menu {:icon-size :big} context)]])


(defn posts [context]
  (let [posts (get-in context [:config :content :posts])
        toggle-tag (fn [tag]
                     (let [tag-str (json/write-str tag)]
                       (format "selected_tags.includes(%s) ? _.pull(selected_tags, %s) : selected_tags.push(%s)"
                               tag-str tag-str tag-str)))]
    [:div.ui.text.container
     [:h1 "Posts"]
     [:div.ui.relaxed.divided.list
      {:x-data "{selected_tags: []}"}
      [:div {:x-text "selected_tags"}]
      (->> posts
           (sort-by :date)
           reverse
           (map
            (fn [{:keys [title path-segment tags] :as post-params}]
              [:div.item
               {:x-show (format "!selected_tags.length || _.intersection(selected_tags, %s).length"
                                (json/write-str tags))}
               [:i.large.file.outline.middle.aligned.icon]
               [:div.content
                [:a {:href (str "/posts/" path-segment)} title]
                [:span.description
                 (components/post-meta
                  post-params
                  {:tag-fn (fn [tag]
                             [:span {"x-on:click" (toggle-tag tag)}
                              tag])})]]])))]]))
