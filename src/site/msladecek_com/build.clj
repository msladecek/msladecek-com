(ns msladecek-com.build
  (:require
   [optimus.assets :as assets]
   [optimus.export]

   [clojure.edn :as edn]
   [stasis.core :as stasis]
   [msladecek-com.pages :as pages]
   [msladecek-com.components :as components]
   [msladecek-com.page-transformers :as transform]))

(defn all-pages [config]
  (let [pages [{:path "/posts/"
                :page-fn pages/posts
                :title "Posts"}
               {:path "/about/"
                :page-fn (transform/render-markdown (slurp (get-in config [:content :about])))
                :title "About"}]
        posts (for [{:keys [file path-segment] :as post-params} (get-in config [:content :posts])]
                (let [file-path (str "content/posts/" file)
                      url-path (str "/posts/" path-segment "/")]
                  (assoc post-params
                         :page-fn (-> (slurp file-path)
                                      (transform/render-markdown-post post-params))
                         :path url-path)))]
    (merge
     {"/" (transform/render-page pages/index {:menu-fn components/navigation-menu
                                              :description (get-in config [:meta :description])})
      "/.well-known/keybase.txt" (slurp (get-in config [:content :keybase-txt]))
      "/robots.txt" (slurp (get-in config [:content :robots-txt]))}
     (into {} (for [{:keys [path page-fn] :as page-params} (concat pages posts)]
                [path (transform/render-page page-fn (merge page-params
                                                            {:menu-fn components/navigation-menu
                                                             :footer-fn components/footer}))])))))

(defn all-assets []
  (concat
   (assets/load-assets "." ["/favicon.ico"])
   (assets/load-assets "styles"
                       ["/custom.css"
                        #"/Fomantic-UI-CSS/.+"
                        #"/pygments/.+"])))

(def config
  (edn/read-string (slurp "content/structure.edn")))

(defn build!
  ([]
   (build! []))
  ([_]
   (let [export-dir "dist"]
     (stasis/empty-directory! export-dir)
     (optimus.export/save-assets (all-assets) export-dir)
     (stasis/export-pages (all-pages config) export-dir {:config config}))))
