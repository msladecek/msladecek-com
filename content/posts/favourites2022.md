In 2022, I've read 53 books, watched 49 (not counting two rewatches of Master & Commander), and played one video game. 
Here are some of my favourites:


## Piranesi

[Piranesi on Goodreads](https://www.goodreads.com/book/show/50202953-piranesi)

I started reading this with no prior info other than it had a high rating on goodreads.

I think this is the right way to go on this one, so I will not tell you what it's about.

Just trust me that it's very good.


## A Deepness in the Sky

[A Deepness in the Sky on Goodreads](https://www.goodreads.com/book/show/226004.A_Deepness_in_the_Sky)

This is the second entry in the _Zones of Thought_ series by Vernor Vinge.
The first one being _A Fire Upon the Deep_.
I really liked the first one and I was completely blown away by the second.

It's a great mix of deep lore, fascinating hard sci-fi and gripping action.

The story is only tangentially related to to A Fire Upon the Deep (there is only one character-related spoiler), so I think they can be read in any order.


## Endurance: Shackleton's Incredible Voyage

[Endurance on Goodreads](https://www.goodreads.com/book/show/139069.Endurance)

Shackleton is an ambitious polar explorer, and there is one last achievement that nobody has claimed: to cross the Antarctic continent on foot and sled.

Everybody's excited, Shackleton finds a ship and a crew and they head out.
But the ship gets stuck in ice so they must switch into survival mode.

The ship is getting crushed by ice, the weather is bad, and food reserves are often uncertain.
They're at the mercy of the currents, hoping that the ice floes will carry them closer to open water, but even if they get lucky, they'll still have to make a harrowing journey across the open ocean in a tiny boat.

It is really an incredible story.

My favourite bit is when Shackleton and his few companions who are on a foot journey to get help for the rest of their expedition need to croos a difficult mountain ridge.
The are packing light so they're not equipped to spend the night high on the mountain.
It is getting late and it is getting colder, if they don't get down soon, they'll freeze to death.
But the way down is treacherous and slippery, they have only a single tool with which to cut a steady path in the ice and the slope below is obscured by mist.
They're out of options, so Shackleton, usually a very cautious man, orders a very bold maneuver.


## Lost Cat

[Lost Cat on Goodreads](https://www.goodreads.com/book/show/55273096-lost-cat)
[Lost Cat on Granta.com](https://granta.com/lost-cat/)

This is an essay by Mary Gaitskill.
She tells the story of how she acquired a kitten while on vacation in Italy and how he got lost once she brought him home.
The story of her looking for the cat is interwoven with a story about a pair of city children who spent their summers at Mary's house.

It's about pain, loss, frustration and reconciliation.

I started reading it knowing nothing about it, and I ended up deeply moved.
One part stuck with me more than others - Mary is arguing with a close person in her life.
She wishes him to be more open and to share with her more private thoughts.
She blames him for the lack of openness and, in frustration, she says she wants a "real" relationship.
Later she realizes that she already has a real relationship, what she was actually asking for was an "ideal" relationship.


## The Soul of a New Machine

[The Soul of a New Machine on Goodreads](https://www.goodreads.com/book/show/7090.The_Soul_of_a_New_Machine)

I learned about this book because it's cover was featured in the last episode of the excellent tv show [Halt and Catch Fire](https://www.imdb.com/title/tt2543312).
It tells the true story of an engineering team builing a new computer in 1980.
It spends a lot of time describing what made the project succesful, what motivated the engineers, and what the managers did to secure support for the project in the corporation.
They built the machine for the sake of building it.
They were motivated by the desire of doing it right.

It made me think a lot about what motivates me personally and what I look for to be happy at my software engineering dayjob.


## Pentiment

[Pentiment on Steam](https://store.steampowered.com/app/1205520/Pentiment/)

Pentiment is a video game in which you play as a 16th century travelling artist who is investigating a series of crimes in a small Bavarian town.
As you're going around town investigating and getting to know everybody, you start understanding that the choices you're making are shaping the world around you. 
It is not possible to be an impartial observer, you have a real impact on this little town.

I was completely blown away by this.
It is obviously closely inspired by The Name of the Rose, which was on my favourites list last year, but it is a very careful homage, not a ripoff.


## RRR

[RRR on IMDB](https://www.imdb.com/title/tt8178634/)

This is just a good old action adventure movie.
Before watching this, I only knew of Indian cinema from the wackiest clips which made it to youtube.
So even though this was quite wacky for my taste, looking back, I don't think it was much more wacky than something like Fast and Furious coming from Hollywood.

RRR is over-the-top, but earnest, and most importantly it's a lot of fun.


## Midnight Mass

[Midnight Mass on IMDB](https://www.imdb.com/title/tt10574558)

Mike Flanagan's series which came out in 2021.
It takes place in an isolated community on an isolated island, which is experiencing some unusual happenings.

It is about people looking for a second chance, people looking for redemption, people looking for purpose.
It is about faith, religion, kindness and forgiveness.
It's melodramatic.
It doesn't impersonate reality, there is no ambiguitiy in the supernatural elements.

Somehow though, it characters seem very real and the story and mystery is very engaging.
I heartily recommend it.


## suckerpinch

[suckerpinch on youtube.com](https://www.youtube.com/@tom7)

My favourite youtube discovery of the year.
The guy's name is Tom.
He has these strange ideas and he pursues them and ends up building strange things.
It's just so full of little technical tidbits and joy, I aspire to be more like Tom in this.
