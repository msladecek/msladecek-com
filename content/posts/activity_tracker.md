Devember is an event organized by the [Level1Techs youtube channel][level1techs-youtube], where they invite members of their community to spend some time in the winter months to working on a side project and to document their progress and share their results with the rest of the community.

The most interesting projects receive prizes from the Level1Techs team.

## My project

My goal was to build a webapp with a simple interface where I could define time based goals and various related clockable activities, and then to clock-in and out of them.
I was hoping end up with a tool which would help me reinforce my habits and give me a better understanding of how I spend my time.

I wanted to build something for myself, something which would be useful in my everyday life.

I wrote about the goals in more details in the [introductory post][first-post] in the level1forum thread.

On the technical side of things, I wanted to learn Clojure and experiment with the [`htmx`][htmx] library for frontend interactivity.
Again, more details and rationale in the [introductory post][first-post-stack].

I ended up submitting weekly updates from October 21st until January 2nd, with a special [retrospective post][retrospective-post] on January 4th and final followup to open discussions on February 22nd.

## Conclusion

I gave my conclusions about how the project went in my [retrospective post][retrospective-post], so I won't repeat all of it here.
Just to summarize a few points:
- Giving weekly status updates helped keep me on track, the flow reminded me of something like SCRUM for defining goals and reviewing them at the end of each cycle.
- Clojure + htmx were a pretty good fit for the project, although I think the clojure code ended up a bit more complicated than it needed to be because I wanted to experiment with various language features.

Most importantly: It's been fun, and I learned a lot.

I got a shoutout in the [finalists video][finalists-video] and a special mention of the quality of my documentation in the [finalists thread on the forum][finalists-thread] which is very gratifying because I put a lot of effort into documenting my progress.

Wendell ended up sending me a couple hundred dollars worth of linode tokens as a prize, for which I'm super grateful.

The code is available on my gitlab: [msladecek/activity-tracker][activity-tracker-gitlab].
I still occasionally push small changes there because I use the tool every day.

[finalists-video]: https://www.youtube.com/watch?v=7-e9abdlTes
[finalists-thread]: https://forum.level1techs.com/t/finalists-for-devember/181620
[first-post]: https://forum.level1techs.com/t/devember-2021-punch-clock-web-app/177270/2#punch-clock-web-app-1
[first-post-stack]: https://forum.level1techs.com/t/devember-2021-punch-clock-web-app/177270/2#stack-7
[retrospective-post]: https://forum.level1techs.com/t/devember-2021-punch-clock-web-app/177270/2#jan-4th-2022-retrospective-1
[activity-tracker-gitlab]: https://gitlab.com/msladecek/activity-tracker
