In 2021, I've read 63 books, watched 56 movies, completed [Advent of Code](https://adventofcode.com/2021), worked on some side projects, got better at software engineering, and watched a bunch of YouTube videos.

Here are some of my favourites:

## Personal projects

### Punch clock application for time tracking

[level1tech forum (Development log)](https://forum.level1techs.com/t/devember-2021-punch-clock-web-app/177270)

In 2021, my goal was to read more.
I really enjoy reading, but whenever I have time for it, I always seem to slide into scrolling twitter or diving into random rabbitholes on youtube.

So I set myself the goal of reading at least one hour per day on average.
I explored some ways of tracking my reading time - first with note taking app on my phone, then google sheets, and finally I built a web application consolidating all of the features I wanted.

I ended up finishing 63 books, but more importantly I gained a better understanding of how much free time I actually have, and how I can better allocate it.

## Books

### Thunder Below!: The USS *Barb* Revolutionizes Submarine Warfare in World War II 

[Goodreads](https://www.goodreads.com/book/show/893889.Thunder_Below_)

[OpenLibrary](https://openlibrary.org/works/OL4123224W/Thunder_Below%21)

This is a really exciting account of Eugene Fluckey's tenure as the captain of the US Submarine Barb, operating in the Pacific towards the tail of the Second World War.

It goes over several combat engagements with Japanese battleships and convoys, describing them from the perspective from inside the sub, command by command, torpedo by torpedo.
It's like an action movie, and it brings a terrific sense of suspense and excitement.

The book also goes over Fluckey's mindset and strategic ambitions in situation of late war Pacific.

I first learned about this book from Destin Sandlin's [video series](https://youtu.be/XFJnWp1tAdU) about the life aboard of a modern US nuclear submarine.

### To Be Taught, If Fortunate

[Goodreads](https://www.goodreads.com/book/show/43190272-to-be-taught-if-fortunate)

[OpenLibrary](https://openlibrary.org/works/OL20642211W/To_Be_Taught_If_Fortunate)

A sci-fi novel - short, but it impacted me deeply.
Takes place aboard a deep-space ship on a mission to investigate life outside of earth.
A team of researchers are put into suspended animation for decades while the ship travels between the stars, then when it reaches one of the destination plantes, they are awoken for a few years to investigate local flora and fauna, and then they have to move on for their next trip.

While only a few years pass for them, decades pass back on Earth, and the crew becomes increasingly disconnected from their old culture.
They find refuge in their work, but that brings certain struggles as well, because the planets vary greatly in the amount of explorable contents - some are overwhelming, and there's not enough time to catalogue everyhting, others are sparse or inaccessible, so the scientists are denied their purpose.

I knew nothing about the story when I picked it up, and as it developed expected it would turn into a more classic sci-fi story about first contact or *there-is-a-monster-aboard*, but instead it turned into a meditation on one's relationship with their work, their passion for learning and humanity.

### Blindsight

[Goodreads](https://www.goodreads.com/book/show/48484.Blindsight)

[OpenLibrary](https://openlibrary.org/works/OL20693012W/Blindsight)

A really dark and strong first contact story by Peter Watts.
Takes place in not-so-distant future, after an anomaly which could only be extraterrestrial in origin lights up Earth's upper atmosphere and forces mankind to rethink its priorities.

A group of astronauts are sent out in an experimental spacecraft to the edge of the solar system to investigate a peculiar object which was detected interacting with a cold gas giant.

The primary crew is composed of various specialists, each of whom pushes the boundaries of what a human is through various physical or mental augmentations.
Also on the crew is Siri Keaton - a specialist in interpreting and communicating ideas of scientists, and Jukka Sarasti - vampiric leader of the expedition who spends most of his time directing others and strategizing with the ship's computer.

Watts explores some terrifying ideas about the nature of sentience, communication and fear itself.

One of the most intriguing components of the novel is the Vampires.
They are until-recently-extinct subspecies of the human race - their altered phisiolagy gives them incredibly strong analytical and predatory skills, but they're unable to produce a certain kind of protein which they must therefore obtain by feeding on humans.

The Vampire lore is really cool and interesting and I didn't do it justice here, fortunately Watts gives more details on it in his [faux pharama conference talk](https://www.youtube.com/watch?v=wEOUaJW05bU).

In a podcast with Neil Blomkamp he talks about how the Vampires were his experiment to take some ridiculous fantasy concept and technobabble it to the point where it made sense in a sci-fi context.
In my opinion, he was successful and the result is really awesome.

I've read most Watts's bibligraphy this year and I'm hooked for more.
There is a consistent theme in his books of a small group of people in dubious mental condition who are isolated from the rest of mankind and have to deal with some great challenge while confronting what it means to be a person.

### The Name of the Rose

[Goodreads](https://www.goodreads.com/book/show/119073.The_Name_of_the_Rose)

[OpenLibrary](https://openlibrary.org/books/OL21189675M/The_Name_of_the_Rose)

In 1327, William, a Franciscan monk, and his Benedictine novice Adso travel to a monastery in northern Italy to prepare for negotiations between the Franciscan order and the representatives of the Pope.
Upon their arrival, the abbot asks them to investigate the recent death of one of the local monks.

As they investigate the mysterious death, we learn about the day-to-day routine of the abbey, the lives and personal histories of the local monks, a grander theological conflict between the religious orders and a power struggle between the Emperor and the Pope.

I was very intimidated by this book, at first glance it seems very dense and tedious, but I was presently surprised by how readable it was.
There are large sections devoted to theological debates, but most if is closely tied to the events of the mystery investigation, and it all build up to a perfect ending.

Out of everything I've read this year, this one would be my strongest recommendation.
I recognize that it might not be as interesting for everyone, so if you struggle with it, maybe you can put it down again for a couple of years.
Maybe it just has to find you at the right time in your life.

### The Garden of Forking Paths

[Goodreads](https://www.goodreads.com/book/show/36436070-the-garden-of-forking-paths)

[OpenLibrary](https://openlibrary.org/books/OL30597973M/Garden_of_Forking_Paths)

This is a collection of short stories by Jorge Luis Borges.

I haven't enjoyed a collection this much since I read *I, Robot*.
When I started reading, I couldn't put it down and when I was done, I couldn't stop thinking about the stories for days after.
I wish I had taken breaks between the stories to let each of them sink in properly.

I'm hooked on it now, so I'll probably read it again in 2022 along with other Borges's collections.

## Movies

### The Kid Detective

[imdb](https://www.imdb.com/title/tt8980602)

Abe is a former prodigious child detective.
He used to solve the town's small mysteries and everybody loved him for it.
Decades later, he's still trying to get by as a private detective, but his career degraded and his business has almost entirely dried out and the townspeople consider him a loser.
One day, a high schooler contracts him to investigate the murder of her boyfriend, and Abe must test all of his detective skills once again.

The style is great, a small town noir.
It's really dry and funny, but also able to deliver tense dramatic moments when it needs to. 

### Pig

[imdb](https://www.imdb.com/title/tt11003218)

Robin, a legendary chef, now forest-dwelling recluse is attacked in the middle of the night and has his prized truffle pig stolen.
He embarks on a quest to recover his friend, which forces him to confront some dubious characters of Portland's culinary underground.

Good performances, Nicolas Cage especially.
The entire film is very pretty and atmospheric, an ode to Portland, cuisine, and the Pacific Nort-West.
It baited me to expect some violent conflict at every moment, but instead Cage's character progressed on his quest through compassion and kindness.

## TV Shows

### Invincible (Season 1)

[imdb](https://www.imdb.com/title/tt6741278)

In a world where superheroes are not entirely uncommon, a teenager is discovering his superpowers which he inherited from his father, the most powerful hero on the planet.

It's a really solid deconstruction of the superhero genre, the VA performances are really great, the characters are fun and likeable and it remains tense and suspensful.

## YouTube

### J. Kenji López-Alt

[YouTube](https://www.youtube.com/c/JKenjiLopezAlt/videos)

This has become my favourite food-related youtube channel.
Kenji records wearing a GoPro on his head.
Unlike other channels, the videos aren't overproduced or too elaborate.
It just feels very accessible, friendly and non-intimidating.

I cooked several things based on them, my favourites are [spaghetti carbonara](https://www.youtube.com/watch?v=k1Np28NnP40) and [mushroom soup](https://www.youtube.com/watch?v=vgIplQn92gU).
Kenji also intersperses the videos with his knowledge from [cooking experiments](https://www.youtube.com/watch?v=hb0Elaa6gxY).

### "How To Speak" by Patrick Winston

[YouTube](https://www.youtube.com/watch?v=Unzc731iCUY)

Patrick Winston ([RIP](https://www.memoriesofpatrickwinston.com/)) was a professor at MIT.
You can find his excellent series of [lectures on Artificial Intelligence also on YouTube](https://www.youtube.com/watch?v=TjZBTDzGeGg&list=PLUl4u3cNGP63gFHB6xb-kVBiQHYe_4hSi).

In "How To Speak" he gives advice on how to construct and deliver a great technical talk or lecture.
Since I've watched this guide, I've been noticing that the principles he talks about are present in many of the that I found interesting over the years, and I've been able to apply some of the advice when presenting my own ideas at work.

### "Everything You Always Wanted to Know about Guano But Were Afraid to Ask" by Daniel Immerwahr

[YouTube](https://www.youtube.com/watch?v=TnI4l6rFuHI)

Really good lecture about the use of guano as fertilizer, the industry it grew around it, and it's role in the history of the United States.
Immerwhahr also talks about the agricultural concerns linked to rapid growth of human population during the industrial revolution, finally pivoting into a personal story about the man who developed a chemical process which made guano obsolete.

### "Mapping Imaginary Cities" and "Minimalist Piano Forever" by Mouse Reeve

[YouTube (Mapping Imaginary Cities)](https://www.youtube.com/watch?v=Ic_5gRVTQ_k)

[YouTube (Minimalist Piano Forever)](https://www.youtube.com/watch?v=ANYMii3Sypg)

Mouse Reeve documents their creative and artful projects in a series (2) of Strange Loop talks.
They reveal interesting stories which inspired the projects and discuss some interesting techniques they've applied - all of this with references to literature for those who wish to dive deeper.

The result radiates joy and passion and makes me want to build stuff like that also.

These represent one of my favourite genres of conference talks - just some person sharing their passion and knowledge with the sole purpose of inspiring others.

### "The Hard Parts of Open Source" by Evan Czaplicki

[YouTube](https://www.youtube.com/watch?v=o_4EX4dPppA)

Evan is the creator of the [Elm programming language](https://elm-lang.org/).
In his Strange Loop talk, he expresses the frustrations he faces as the maintainer of Open Source projects, primarily related to how incosiderate people communicate their grievances.

He talks about the subject without malice and links the patterns he percieves to themes which have been present in the software engineering culture for many decades.
He also proposes adoption of a different mindset - one focused on deliberately seeking mutual understanding.
Although he find himself doing thankless work, he delivers his thesis with the same mild-mannered compassion he argues for.

In my opinion, the analysis and advice in this talk is universal, certainly not limited to the discussions in Open Source projects. 

## Plans for 2022

Continue reading at least 1hr a day.
More [Jorge](https://www.goodreads.com/book/show/16568.The_Book_of_Imaginary_Beings) [Luis](https://www.goodreads.com/book/show/426504.Ficciones) [Borges](https://www.goodreads.com/book/show/5787.The_Aleph_and_Other_Stories).

Learn some frontend engineering skills; Build something with the [Elm programming language](https://elm-lang.org/), [htmx](https://htmx.org/), and perhaps [svelte-kit](https://kit.svelte.dev/).

Continue building stuff with clojure, experiment in particular with [`core.logic`](https://github.com/clojure/core.logic) and [`core.match`](https://github.com/clojure/core.match).

Use the time tracking application for more activities than reading books to improve my understanding of my free time even more.
