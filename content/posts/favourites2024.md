## Pushing Ice

[Pushing Ice on Goodreads](https://www.goodreads.com/book/show/89186.Pushing_Ice)

This is a really fantastic first contact story set in a not-so-distant future.
Janus, one of Saturn's moons suddenly changes its orbit and sets on a course out of the solars system.
The only spaceship that can reach it in time is an ice mining ship, the Rockhopper.

It gives me strong Rendezvous with Rama vibes, although thoroughly modernized.
The characters are great and there is a lot of interesting conflict among the crew itself as well as a lot of mysteries related to the travelling moon, its origin, and its purpose.

## Bobby Fingers

[Bobby Fingers on YouTube](https://www.youtube.com/@bobbyfingers)

Bobby is an incredibly skilled and funny guy who makes highly detailed dioramas and tells stories.
I cannot possibly do the channel justice in just a couple of sentences, just give it a watch and you'll quickly see if it's your jam.
To me, it's fascinating to watch somebody leverage so much talent and hard work in such a charming and silly way.

## For All Kerbalkind

[For All Kerbalkind (playlist) on YouTube](https://www.youtube.com/playlist?list=PL7TVzn8CoIqnZE7fxcAix46pxWzx0nWf3)
Two youtubers compete in a simulated space race.
Although this series has been going on since 2020, I only stumbled upon it in February 2024 and I was instantly hooked.
[Beardy Penguin](https://www.youtube.com/@TheBeardyPenguin) plays as the Soviet Union and [N9 Gaming](https://www.youtube.com/@N9GamingOfficial) as the United States in a heavily-modded, role-play focused version of [Kerbal Space Program](https://store.steampowered.com/app/220200/Kerbal_Space_Program/).
Their technological progression and mission profiles follows real history to some extent, but they're by no means constrained by it, which makes for a captivating storyline.
Unlike in vanilla KSP, they must account for vehicle assembly time and manufacturing capacity, engine failures, and interference from one another.
They also both put a lot of effort into presentation of their videos, going date by date explaining their strategy and progress.
The mods they've chosen drastically improve the graphics compared to the base game as well and it's just joy to watch.

The closest analogue for something like this that I can think of would be a long-running D&D campaign or similar episodic semi-improvised series.

## Severance

[Severance on IMDB](https://www.imdb.com/title/tt11280740)

Clever mystery show about workers in an unusual office.
That's all I'm going to say about the plot, because I think it's best to enjoy it with as little knowledge as possible.

The first season came out in 2022 but I only got around to watching it this year.
It kept me on the edge of my seat so I binged it over a weekend.

