## Magellan

[Magellan on Goodreads](https://www.goodreads.com/book/show/1393716.Magellan)

Written by Stefan Zweig, this is the story of Ferdinand Magellan - a Portuguese sailor, who set for himself the goal of finding a passage around South America and to circumnavigate the globe.

Zweig portrays Magellan as a hero with an iron will and determination.
From this perspective, the nay-sayers, mutineers and various other antagonists are lesser men.
Magellan is shown as a clever strategist, who outsmarts his enemies and delivers success that nobody else could.

I think he could also be seen as a stubborn risk taker, who refused to take advantage of the counsel of other experienced sailors.
He concealed critical information from them out of paranoia, and set out on a trans-oceanic voyage without adequate provisions, for which his crew payed dearly.

## Draft No. 4

[Draft No. 4 on Goodreads](https://www.goodreads.com/book/show/18194765-draft-no-4)

The author, John McPhee is an accomplished non-fiction writer.
This book is a retrospective of some of his notable projects and a collection of various advice on writing.

To me, McPhee's writing is like magic.
I find any topic he writes about instantly compelling even though I wouldn't expect it beforehand.
It's interesting to take a peak behind the curtain.

## 17776 and 20020

[17776 on Secret Base](https://www.sbnation.com/a/17776-football)

[20020 on Secret Base](https://www.sbnation.com/c/secret-base/21410129/20020)

Jon Bois's fabulous piece of speculative fiction, a love letter to football and its weird byzantine rules.

It is the year 17776 and Pioneer 9 - a deep space probe becomes conscious.
It is able to converse with a couple other probes which have become sentient over the millennia and learns that on Earth,
there is no more aging, no more dying and no more strife.
People in the United States occupy themselves by playing increasingly weird variants of football.

It's really fun and the quasi-interactive presentation inside of the webpage is very clever.

20020 is a sequel to 17776 and in my opinion it even surpasses it.
This time the story focuses on one particular pair of players in one particular game of weird football.
Once again the text of the story is interspersed with Jon Bois's signature google earth montages.

Watch the [introduction video for 20020](https://www.youtube.com/watch?v=C1JNQKNAvNY) to get an idea of the tone and style.

## Disco Elysium

[Disco Elysium on Steam](https://store.steampowered.com/app/632470/Disco_Elysium__The_Final_Cut)

This game originally came out in 2019 and I've always heard only good things about it but I never got around to playing it myself.
Last year I really enjoyed Pentiment.
Josh Sawyer, Pentiment's director, cites Disco Elysium as one of the main influences for Pentiment's game mechanics, so I decided to give it a try.

I love Disco Elysium.
It's a clever, funny and deeply original murder mystery, set in a strange world with tragic history.
We play as a detective on a murder case, who lost all of his memories including the knowledge of his own name.
With him, we learn about the world, who he used to be, and we choose how he'll affect the world around him and the people who live in it.

The core mechanics are driven by dialogue happening in the detective's head between various aspects of his personality.
These aspects also play the role of skills or abilities, and as we level them up, they become more prominent in our internal dialogue and they affect what options we have in the world around us.

It's amazing, very funny and engaging, the art style is beautiful, the environment is beautiful, the soundtrack is beautiful. I cannot recommend it enough.

## Scavengers Reign

[Scavengers Reign on IMDB](https://www.imdb.com/title/tt21056886)

After an interstellar freighter crashes on a strange planet, the remnants of its crew do what they can to survive.
The life is strange here, the distinction between flora and fauna is not completely clear.
Hundreds of variations of dangerous weird little guys are all around the place.
It's completely mesmerizing.

## Blue Eye Samurai

[Blue Eye Samurai on IMDB](https://www.imdb.com/title/tt13309742)

Very stylish and brutal animated take on the Samurai/Ronin genre.
I started watching the first episode not knowing anything about it, and ended up binging the whole first season in a single session.

The entire first episode is [available on youtube](https://www.youtube.com/watch?v=Cm73ma6Ibcs).

## Silicone ducky night light

[Benson Duck Light on undee.cc](https://uneede.cc/products/uneede-led-duck-night-light-cute-animal-silicone-nursery-night-light-rechargeable-table-lamp-bedside-lamp-with-touch-sensor-for-women-bedrooms-living-room)

This is one of my favourite things I ever got for Christmas.
It's this little silicone duck lamp.
It doesn't have a regular activation button, instead it lights up when you squish it, or tap it.

It gives out warm, gentle light and the squish is very satisfying.
It's a very pleasant and friendly object.
