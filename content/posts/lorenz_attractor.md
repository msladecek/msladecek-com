The story is that Edward Lorenz, a mathematician and a meteoroligst, was studying the output of a simulation of weather patterns.

This was the year 1961, so the output of such simulation was a paper sheet listing the values of each state variable row by row representing the steps of the simulation.

After he gathered some data, he reran the simulation, but this time he used values from the middle of the printout as the initial state and found out that the results soon began to diverge from the previous run.

The model was a system of ordinary differential equations, there was no intended randomness so it ought to have been completely deterministic, yet the results were different.

The reason for the discrepancy was that the computer printed out values with three digits of precision, but internally it used six digits.

That subtle difference caused by the rounding in the initial conditions had major consequences on the state of the system after several iterations of the simulation.


The Lorenz Attractor is a simplified version of the system, but it still exhibits the same chaotic properties. (And it's also very pretty.)
There are three state variables which can be interpreted as coordinates in three dimensional space.

My simulation runs multiple points with very similar starting positions, all controlled by the same set of differential equations.
At first they seem to follow the same path but then they suddenly diverge.

They never leave completely though, it is clear that there is a structure expressed in those formulas.
Like little frictionless particles, they orbit in a strange attraction field.

## Demo

I built the simulation with clojurescript and [`quil`][quil].
The structure formed by the Lorenz attractor is three dimensional, but I draw only a 2D projection.
The `quil` library does have some support for 3D visualizations, but I wasn't able to make it work in a satisfying way so I just added a couple keyboard controls to allow for switching the projection perspective.

<iframe
    src="https://lorenz-attractor.msladecek.com"
    width="100%"
    height="600px"
    >
</iframe>

Several keybindings are available to adjust the simulation (you may have to first click on the simulation frame so that the browser will capture they keyboard events).

Use the `j` and `k` keys to shorten and extend the trails, the `h` and `l` keys to switch the perspective, `r` and `R` to reset back to initial state (each option gives a slightly different state) and `d` to switch between dark and light backgrounds. The `?` key will show some debug information as well as the list of keybindings.

Try it with the shortest trails possible, it will appear as a couple of little particles playing around trying to chase one another.
If you extend the trails though, you'll begin to see the mesmerizing loops and turns of the attraction field. 

A fullscreen version is available on [lorenz-attractor.msladecek.com](https://lorenz-attractor.msladecek.com) and the code on my gitlab:  [msladecek/lorenz-attractor][gitlab-lorenz].

The color palette is [`gruvbox`][gruvbox], but you'll find a [couple of others][gitlab-lorenz-palette] defined in the codebase if you wish to play with it.

[gruvbox]: https://github.com/morhetz/gruvbox
[quil]: http://www.quil.info/
[gitlab-lorenz]: https://gitlab.com/msladecek/lorenz-attractor
[gitlab-lorenz-palette]: https://gitlab.com/msladecek/lorenz-attractor/-/blob/master/src/app/lorenz_attractor/core.cljs?ref_type=heads#L6
