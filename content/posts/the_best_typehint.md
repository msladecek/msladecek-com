I'm not a fan of python typehints.
I find that half the time they don't let me say the things I want to say about the data flowing through my program.
The other half of time they make me spend time solving constraint puzzles instead of getting actual work done.

There is one python feature however, which although it is not a typehint itself, tells me something very valuable about the "types" of things my program and often leads me to a better structure.

I'm talking about the `async` and `await` keywords.

In a program which utilizes python's [`asyncio`][python-asyncio], these keywords are required at coroutine definition and call site respectively.
Coroutines are `asyncio`'s way of organizing code IO-dependant code, allowing it to run concurrently instead of blocking the main thread.
As such, when I see code wrapped in `async/await` I immediately recognize it as IO-dependant and an alarm bell triggers in my brain asking me if it really needs to be so, or if I can push it away and factor out some of the logic to a pure, non-async function.

The best kind of function is a pure function - one with no side effect, where the output depends only on the inputs.
A coroutine touches the outside world, so it's not a pure function either because it reads from the outside world to generate the output, or because it writes side effects into the outside world.

Consider and example like this:

```python
async def validate_periscope_command(command_payload):
    source = command_payload["source"]
    if source not in {"captain", "navigator"}:
        await log_security_incident(command_payload)
        return {"command": command_payload ,"is_valid": False}

    return {"command": command_payload ,"is_valid": True}

async def validate_nuclear_missiles_command(command_payload):
    source = command_payload["source"]
    if source not in {"captain"}:
        await log_security_incident(command_payload)
        return {"command": command_payload ,"is_valid": False}

    return {"command": command_payload ,"is_valid": True}

async def handle_command(command_payload):
    if command_payload["command"] == "up periscope" or command_payload["command"] == "down periscope":
        return await validate_periscope_command(command_payload)
    elif command_payload["command"] == "fire the nuclear missiles":
        return await validate_nuclear_missiles_command(command_payload)
    else:
        return {"command": command_payload, "is_valid": False}
```

Now that's an awful lot of coroutines, just because `handle_command` delegates down to the two different cases which must also be coroutines because they might need to call `log_security_incident`, which is presumably writing to some external storage.
So `validate_periscope_command`, `validate_nuclear_missiles_command` are infected by the impurity even though their core responsibility - validating the business rule of who can issue which command - doesn't depend on anything external.

Let's instead extract the business logic into a separate functions:

```python
def validate_periscope_command(command_payload):
    source = command_payload["source"]
    return source in {"captain", "navigator"}

def validate_nuclear_missiles_command(command_payload):
    source = command_payload["source"]
    return source in {"captain"}

async def handle_command(command_payload):
    if command_payload["command"] == "up periscope" or command_payload["command"] == "down periscope":
        command_is_known = True
        command_is_valid = validate_periscope_command(command_payload)
    elif command_payload["command"] == "fire the nuclear missiles":
        command_is_known = True
        command_is_valid = validate_nuclear_missiles_command(command_payload)
    else:
        command_is_known = False
        command_is_valid = False

    if command_is_known and not command_is_valid:
        await log_security_incident(command_payload)

    return {"command": command_payload, "is_valid": command_is_valid}
```

We've made `handle_command` a bit heavier, but in return we've made the two business rules, `validate_periscope_command` and `validate_nucleare_missiles_command` much lighter.
In this case the rules are so simple we could reasonably inline them into the main validator coroutine, but for the sake of the argument we can imagine much more involved and complicated business rules in their place.

The point is that in this example we're going beyond just deduplication of the security incident logging logic.
We're reducing the burden on the business-critical components, so they may be understood (and tested) in isolation.
And we're making it explicit through the use of `async/await` keywords so that the person reading the code doesn't have to know the details of either component to get an understanding of their dependency on the outside world.

To be fair, there are other ways to do IO in python outside of `asyncio`, so we must still exercise a certain amount of caution and discipline.

### Bonus idea: Function arguments can be typehints too!

Consider an example like this:
```python
db = connect_database(DATABASE_URL)

def assign_ticket(user, ticket):
    db.execute("UPDATE ticket SET assignee_id = ? WHERE id = ?", ticket.id, user.id)

def handle_new_tickets(tickets):
    with HTTPClient() as client:
        users = client.get_users()

    for ticket in tickets:
        random_user = random.choice(users)
        assign_ticket(user, ticket)
```

Versus this:

```python
def assign_ticket(db, user, ticket):
    db.execute("UPDATE ticket SET assignee_id = ? WHERE id = ?", ticket.id, user.id)

def handle_new_tickets(db, http_client, tickets):
    users = http_client.get_users()

    for ticket in tickets:
        random_user = random.choice(users)
        assign_ticket(user, ticket)
```
In the first case the functions have implicit dependencies on the database and on http connectivity, and in the second the dependencies are made explicit.

My point is this: Isn't a function that makes an http request a different *type* of thing than a function that doesn't? Isn't a function that triggers a database query (even by delegating to another function) a different *type* of thing still?

[python-asyncio]: https://docs.python.org/3/library/asyncio.html
