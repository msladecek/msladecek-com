# About me

My name is Martin.

I'm passionate about functional programming (the pragmatic Clojure kind) and letting data do the heavy lifting (the [Data Oriented Programming][data-oriented-programming] kind).

At work I use Python, SQL and occasionally JavaScript and TypeScript,
but I always enjoy exploring and learning new programming paradigms and languages.

I'm always trying to find robust models of data, information and knowledge.

I enjoy interacting with domain experts or the users themselves to develop the best solutions possible in tight feedback loops.

I believe that frameworks are better than platforms, libraries are better than frameworks, and recipes are better than libraries.

I want to eat my own dogfood and drink my own champagne.
I find it hard to become passionate about a software project if I don't understand its real-world benefits.

Just as woodworkers and metalworkers build and adjust their tooling using their professional skills, so should software developers. 
We should not tolerate tooling that is not modular, extensible and repairable.
We should not shy away from building our own tooling.

I'm trying to get better at sharing the stuff I learn although I post on this site only infrequently.

For social media, I use [twitter][twitter] and [mastodon][mastodon], although not very actively.

## About this site

Hand-crafted with Clojure, [stasis][github-stasis] and [Fomantic-UI][github-fomantic-ui].
The source is available on my [gitlab][gitlab-msladecek-com].
All content on this site was written by a fully organic human being and is 100% *AI*-free.

[github-stasis]: https://github.com/magnars/stasis
[github-fomantic-ui]: https://github.com/fomantic/Fomantic-UI
[gitlab-msladecek-com]: https://gitlab.com/msladecek/msladecek-com
[twitter]: https://twitter.com/martin_sladecek
[mastodon]: https://hachyderm.io/@msladecek
[data-oriented-programming]: https://www.manning.com/books/data-oriented-programming
